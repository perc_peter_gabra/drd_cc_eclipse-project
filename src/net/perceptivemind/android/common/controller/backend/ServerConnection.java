package net.perceptivemind.android.common.controller.backend;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import android.graphics.BitmapFactory;

import com.perceptivemind.template.helper.Logger;
import com.perceptivemind.template.helper.Utilities;

public class ServerConnection
{
	public enum ResponseType{
		RESP_TYPE_BYTE_ARRAY,
		RESP_TYPE_STRING,
		RESP_TYPE_INPUT_STREAM
	}
	
	String serverUrl = "";
	DefaultHttpClient httpClient = null;
	HttpGet httpGet = null;
	HttpPost httpPost = null;
	HttpDelete httpDelete = null;

	// Default Constructor
	public ServerConnection()
	{
	}

	
	/*
	 * Send Request to the server with the headers
	 */
	public CTHttpResponse sendRequestToServer(String url, String methodType, final String contentType,
	  final HashMap<String, String> additionalHeaders, final HttpEntity bodyEntity, final String basicAuth, final ResponseType responseType)
	{
		Logger.instance()
		  .v(
		    "sendRequestToServer",
		    url + " " + ((bodyEntity != null) ? bodyEntity.toString() + "  " + bodyEntity.getContentLength() : "Null")
		      + "  ", true);

		CTHttpResponse serverResponse = new CTHttpResponse();
		
		// HttpURLConnection con = null;
		HttpResponse httpResponse = null;
		try
		{
			httpClient = getNewHttpClient();
			httpClient.addRequestInterceptor(new HttpRequestInterceptor()
			{
				@Override
				public void process(HttpRequest request, HttpContext context) throws HttpException, IOException
				{
					if (!Utilities.isNullString(contentType)) 
						request.setHeader(HTTP.CONTENT_TYPE, contentType);
					
					if (bodyEntity != null){
						request.setHeader(HTTP.CONTENT_LEN, String.valueOf(bodyEntity.getContentLength()));
					}
					
					if(!Utilities.isNullString(basicAuth))
						request.setHeader("Authorization", basicAuth);
					
					if(additionalHeaders != null){
						Iterator<String> headersKeys = additionalHeaders.keySet().iterator();
						while(headersKeys.hasNext()){
							String key = headersKeys.next();
							request.setHeader(key, additionalHeaders.get(key));
						}
					}
				}
			});
			
			if (methodType.compareToIgnoreCase(HttpPost.METHOD_NAME) == 0)
			{
				httpPost = new HttpPost(url);
				
				if (bodyEntity != null)
				{
					httpPost.setEntity(bodyEntity);
				}
				httpResponse = httpClient.execute(httpPost);
			}
			else if (methodType.compareToIgnoreCase(HttpGet.METHOD_NAME) == 0)
			{
				httpGet = new HttpGet(url);
				httpResponse = httpClient.execute(httpGet);
			}
			else if (methodType.compareToIgnoreCase(HttpDelete.METHOD_NAME) == 0)
			{
				httpDelete = new HttpDelete(url);
				httpResponse = httpClient.execute(httpDelete);
			}

			if (httpResponse != null)
			{
				org.apache.http.Header[] headers = httpResponse.getAllHeaders();
				if(headers != null)
				{
					serverResponse.responseHeaders = new HashMap<String, String>();
					
					for(org.apache.http.Header header : headers)
					{
						System.out.println(header.getName()+" => "+header.getValue());
						
						if(!Utilities.isNullString(header.getName()) && !Utilities.isNullString(header.getValue())) 
							serverResponse.responseHeaders.put(header.getName(), header.getValue());
						
						if(!Utilities.isNullString(header.getName()) && header.getName().compareToIgnoreCase("Error-Code") == 0)
							serverResponse.statusCode = Integer.valueOf(header.getValue());
					}
				}
				serverResponse.statusMessage = httpResponse.getStatusLine().getReasonPhrase(); // CTApplication.getContext().getString(R.string.conn_problem);
				                                                              
				if(serverResponse.statusCode <= 0)
					serverResponse.statusCode = httpResponse.getStatusLine().getStatusCode();
				// When Succeeded
				if (serverResponse.statusCode == HttpStatus.SC_OK || 
					serverResponse.statusCode == HttpStatus.SC_CREATED ||
					serverResponse.statusCode == HttpStatus.SC_ACCEPTED)
				{
					// Scanner scan1 = new Scanner(con.getInputStream());
					// LocintouchLogger.instance().v("scan1.hasNext()", scan1.hasNext()+" "+scan1,true);

					// String line = IOUtils.toString(httpResponse.getEntity().getContent());
					InputStream inputStream = httpResponse.getEntity().getContent();
					if (inputStream != null)
					{
						if(responseType != null && responseType == ResponseType.RESP_TYPE_INPUT_STREAM)
						{
							serverResponse.response = inputStream;
						}
						else
						{
							BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
							if (reader != null)
							{
								int length = (int) httpResponse.getEntity().getContentLength();
						
								StringWriter total = null;
								if (length > 0)
									total = new StringWriter(length);
								else 
									total = new StringWriter();
								
								Logger.instance().v("ServerConn", ".. Start Reading Response.. " + httpResponse.getEntity().getContentLength(), false);
								
								String line;
								while ((line = reader.readLine()) != null)
								{
									total.append(line + "\n");
									line = null;
								}
								line = total.toString();
								total = null;
	
								if(!Utilities.isNullString(line))
								{
									line = line.replaceAll("Notice: Undefined index: role in /home/prod/module/Application/src/Application/Model/User.php on line 181", "").trim();
									line = line.replaceAll("Notice: Undefined index: number in /home/prod/module/Application/src/Application/Model/Booking.php on line 293", "").trim();
								}
								serverResponse.response = line;
								line = null;
								reader = null;
								inputStream = null;
								// scan1.close();
							}
						}
					}
				}
				// when failed
				else
				{
					// InputStream inputStream = con.getErrorStream();
					Logger.instance()
					  .v("Not Succeded(!200)",
					    httpResponse.getStatusLine().getStatusCode() + "   " + httpResponse.getStatusLine().getReasonPhrase(),
					    true);
					InputStream inputStream = httpResponse.getEntity().getContent();
					if (inputStream != null)
					{
						String respError = IOUtils.toString(inputStream);
						if (!Utilities.isNullString(httpResponse.getStatusLine().getReasonPhrase()))
							serverResponse.errorMessage = httpResponse.getStatusLine().getReasonPhrase();
						else 
							serverResponse.errorMessage = respError;
						Logger.instance().v("Error ->", respError, false);
					}
				}
			}
		}
		// Catch if there
		catch (Exception e)
		{
			try
			{
				if(serverResponse.statusCode <= 0)
					serverResponse.statusCode = (httpResponse != null) ? httpResponse.getStatusLine().getStatusCode() : -1;
			}
			catch (Exception e1)
			{
				if (e1 instanceof org.apache.http.conn.ConnectTimeoutException)
				{
					serverResponse.statusCode = HttpStatus.SC_REQUEST_TIMEOUT;
//					response[2] = CTApplication.getContext().getString(R.string.conn_time_out);
				}
				else
				{
					serverResponse.statusCode = -1;
					serverResponse.errorMessage = null;
				}
				Logger.instance().v("sendRequestToServer()--", e1, true);
			}

			serverResponse.response = null;
			if (e instanceof org.apache.http.conn.ConnectTimeoutException)
			{
				serverResponse.statusCode = HttpStatus.SC_REQUEST_TIMEOUT;
//				response[2] = CTApplication.getContext().getString(R.string.conn_time_out);
			}
			Logger.instance().v("sendRequestToServer()", e, true);
		}

		finally
		{
			System.gc();
		}
		
		Logger.instance().logFullMessage("Request End", serverResponse.statusCode+" "+serverResponse.response, false);
		return serverResponse;
	}
	

	public CTHttpResponse downloadImage(String imageUrl, final HashMap<String, String> requestAdditionalHeaders, final int requiredSize)
	{
		CTHttpResponse serverResponse = new CTHttpResponse();
		
		DefaultHttpClient httpClient = ServerConnection.getNewHttpClient();
		if (requestAdditionalHeaders != null)
		{
			httpClient.addRequestInterceptor(new HttpRequestInterceptor()
			{
				@Override
				public void process(HttpRequest request, HttpContext context) throws HttpException, IOException
				{
					Iterator<String> headersKeys = requestAdditionalHeaders.keySet().iterator();
					while (headersKeys.hasNext())
					{
						String key = headersKeys.next();
						request.setHeader(key, requestAdditionalHeaders.get(key));
					}
				}
			});
		}
		HttpGet httpGet = new HttpGet(imageUrl);
		try
		{

			HttpResponse response = httpClient.execute(httpGet);

			// check 200 OK for success
			final int statusCode = response.getStatusLine().getStatusCode();
			serverResponse.statusCode = statusCode;
			if (statusCode != HttpStatus.SC_OK)
			{
				Logger.instance().v("ImageDownloader",
				  "Error " + statusCode + " while retrieving bitmap from " + imageUrl + " Status code " + statusCode, false);
				return serverResponse;
			}

			final HttpEntity entity = response.getEntity();
			if (entity != null)
			{
				InputStream inStream = null;
				try
				{
					// getting contents from the stream
					inStream = entity.getContent();

					// Copy instream for decode twice
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int len;
					while ((len = inStream.read(buffer)) > -1)
					{
						baos.write(buffer, 0, len);
					}
					baos.flush();
					InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
					InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

					BitmapFactory.Options o = new BitmapFactory.Options();
					o.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(is1, null, o);

					int scale = 1;
					if (o.outHeight > requiredSize || o.outWidth > requiredSize)
					{
						scale = (int) Math.pow(2,
						  (int) Math.round(Math.log(requiredSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
					}

					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					serverResponse.response = BitmapFactory.decodeStream(is2, null, o2);
				}
				finally
				{
					if (inStream != null)
					{
						inStream.close();
					}
					entity.consumeContent();
				}
			}
		}
		catch (Exception e)
		{
			// You Could provide a more explicit error message for IOException
			httpGet.abort();
			Logger.instance().v("ImageDownloader",
			  "Something went wrong while" + " retrieving bitmap from " + imageUrl + "  " + e.toString(), false);
		}

		return serverResponse;
	}
	public static DefaultHttpClient getNewHttpClient()
	{
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			org.apache.http.conn.ssl.SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used.
			int timeoutConnection = 20000;
			HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 40000;
			HttpConnectionParams.setSoTimeout(params, timeoutSocket);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			// HttpHost PROXY_HOST = new HttpHost("5.39.79.171 : 3128", 3128);
			DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);
			// httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, PROXY_HOST);
			return httpClient;
		}
		catch (Exception e)
		{
			return new DefaultHttpClient();
		}
	}

	/*
	 * To Accept SSL Connections
	 */
	static class MySSLSocketFactory extends org.apache.http.conn.ssl.SSLSocketFactory
	{
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException,
		  KeyStoreException, UnrecoverableKeyException
		{
			super(truststore);

			TrustManager tm = new X509TrustManager()
			{
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{
				}

				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{
				}

				public X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}
			};

			sslContext.init(null, new TrustManager[]
			{ tm }, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException,
		  UnknownHostException
		{
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException
		{
			return sslContext.getSocketFactory().createSocket();
		}
	}

	public void cancelConnection(){
		if(httpGet != null)
			httpGet.abort();
		if(httpPost != null)
			httpPost.abort();
		if(httpDelete != null)
			httpDelete.abort();
		if(httpClient != null){
			httpClient.getConnectionManager().shutdown();
		}
	}
}
