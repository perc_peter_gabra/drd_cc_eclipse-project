package net.perceptivemind.android.common.controller.backend;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;

public class ErrorDialog
{
	public static AlertDialog alert;
	
	public static void showMessageDialog(final String title, final String message, final Activity activity){
		showMessageDialog(title, message, activity, null,null);
	}
	public static void showMessageDialog(final String title, final String message, final Activity activity, final OnClickListener onclick){
		showMessageDialog(title, message, activity, null,onclick);
	}
	
	public static void showMessageDialog(final String title, final String message, final Activity activity, final Runnable runnable,final OnClickListener onclick)
	{
		if(activity == null) return;
		activity.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
				dialog.setTitle(title);
				dialog.setMessage(message);
				dialog.setCancelable(false);
				if(onclick != null)
					dialog.setPositiveButton(activity.getString(android.R.string.ok), (android.content.DialogInterface.OnClickListener) onclick);
				else
				dialog.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int whichButton)
					{
						if(runnable!= null){
							runnable.run();
						}
					}
				}); 

				try
        {
					if(alert != null && alert.isShowing())
						alert.dismiss();
					alert = dialog.create();
					alert.show();
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        }
			}
		});
	}
}
