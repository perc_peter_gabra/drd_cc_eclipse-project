package net.perceptivemind.android.common.controller.backend;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import net.perceptivemind.android.common.controller.backend.ServerConnection.ResponseType;

import org.apache.http.HttpEntity;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.perceptivemind.template.CTApplication;
import com.perceptivemind.template.R;
import com.perceptivemind.template.backend.ServerKeys;
import com.perceptivemind.template.helper.Utilities;
import com.perceptivemind.template.managers.Engine;
import com.perceptivemind.template.model.Consts;

public abstract class BaseOperation extends AsyncTask<Object, Object, Object>
{
	public static final int UNHANDLED_EXCEPTION_STATUS_CODE = 1001;

	ArrayList<RequestObserver> observersList;

	ServerConnection serverConnection;

	boolean isShowLoadingDialog = true;
	public boolean isRunning = false;
	Activity activity;
	private ProgressDialog dialog;
	int requestID = 0;
	

	public BaseOperation(int requestID, boolean isShowLoadingDialog, Activity activity)
	{
		this.isShowLoadingDialog = isShowLoadingDialog;
		this.activity = activity;
		this.requestID = requestID;

		serverConnection = new ServerConnection();
		observersList = new ArrayList<RequestObserver>();
	}

	/**
	 * Do/Execute the operation itself
	 * 
	 * @return the object
	 * @throws Exception
	 */
	public abstract Object doMain() throws Exception;

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		if (isShowLoadingDialog)
		{
			dialog = new ProgressDialog(activity);
			dialog.setMessage(activity.getString(R.string.wait_moment));
			dialog.setTitle(activity.getString(R.string.loading));
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
//			if (!dialog.isShowing()) dialog.show();

			dialog.setOnCancelListener(new OnCancelListener()
			{
				@Override
				public void onCancel(DialogInterface dialog)
				{
					Log.v("Dialog", "Canceled ");
					cancelConn();
					// Wake observers with the result
					for (RequestObserver observer : observersList)
					{
						observer.requestCanceled(requestID, null);
					}
					BaseOperation.this.cancel(true);
					
				}
			});

			try
      {
				if (!dialog.isShowing())
					dialog.show();
      }
      catch (Exception e)
      {} // Show dialog on activity killed
		}
	}

	@Override
	protected Object doInBackground(Object... params)
	{
		Object object = null;
		try
		{
			isRunning = true;
			object = doMain();
			
		}
		catch (Throwable t)
		{
			if (!(t instanceof CTHttpError)) t.printStackTrace();
			return t;
		}
		return object;
	}

	@Override
	protected void onCancelled()
	{
		super.onCancelled();
		cancelConn();
	}

	public void cancelConn() {
		dismiss();
		disconnect();
	}

	public void disconnect()
	{
		if(serverConnection != null) 
			serverConnection.cancelConnection();
	}

	@Override
	protected void onPostExecute(Object result)
	{
		super.onPostExecute(result);
		dismiss();
		isRunning = false;

		// Wake observers with the result
		for (RequestObserver observer : observersList)
		{
			Throwable error = (result instanceof Throwable) ? (Throwable) result : null;
			Object object = (result instanceof Throwable) ? null : result;
			observer.handleRequestFinished(requestID, error, object);
		}
	}
	
	private void dismiss()
	{
		try
    {
			if (isShowLoadingDialog && dialog.isShowing()) dialog.dismiss();
    }
    catch (Exception e)
    {} // Dismiss on activity killed
	}
	
	protected void onUpdated(Object object)
  {
		// Wake observers with the result
		for (RequestObserver observer : observersList)
		{
			observer.updateStatus(requestID, object);
		}
  }

	/**
	 * Execute http request
	 * 
	 * @param requestUrl
	 *          URL for the request
	 * @param methodType
	 *          GET/POST/PUT etc..
	 * @param contentType
	 *          Content Type
	 * @param additionalHeaders
	 *          any headers to be applied for the request
	 * @param bodyEntity
	 *          if it was a Post request and need a body
	 * @param responseType
	 *          Byte/String
	 * @param basicAuth
	 *          Authentication String
	 * @return the CTHttpResponse object
	 */
	public CTHttpResponse doRequest(String requestUrl, String methodType, final String contentType,
	  final HashMap<String, String> additionalHeaders, final HttpEntity bodyEntity, String basicAuth,
	  final ResponseType responseType)
	{
		CTHttpResponse response = serverConnection.sendRequestToServer(requestUrl, methodType, contentType,
		  additionalHeaders, bodyEntity, basicAuth, responseType);
		

		if(Utilities.isNullString(basicAuth) || !basicAuth.equalsIgnoreCase(Consts.KEYS_IGNORE_VALIDATION))
			ensureHTTPRequestSucceeded(this, response);

		return response;
	}

	/*
	 * ******************************************************************
	 * ********************** Observers Handling ************************
	 * ******************************************************************
	 */
	/*
	 * Add Request Observer to List
	 */
	public void addRequsetObserver(RequestObserver requestObserver)
	{
		// remove the observer if it was already added here
		removeRequestObserver(requestObserver);
		// add to observers List
		observersList.add(requestObserver);
	}

	/*
	 * Remove Request Observer from the list
	 */
	public void removeRequestObserver(RequestObserver requestObserver)
	{
		observersList.remove(requestObserver);
	}

	// //////////////// End of observers handling /////////////////////

	/*
	 * Check if the response is Valid HTTP Response
	 */
	protected static void ensureHTTPRequestSucceeded(BaseOperation operation, CTHttpResponse response)
	{
		if (response == null)
		{
			throw new RuntimeException("Invalid Response Object while processing operation ["
			  + operation.getClass().getName() + "]");
		}

		if (response.statusCode != HttpURLConnection.HTTP_OK && response.statusCode != HttpURLConnection.HTTP_CREATED
		  && response.statusCode != HttpURLConnection.HTTP_ACCEPTED)
		{
			throw new CTHttpError(response.statusMessage, response.statusCode);
		}
	}

	/*
	 * Check if the JSON response is succeeded
	 */
	protected static void ensureRequestSucceeded(JSONObject responseJSON)
	{
		if (responseJSON != null)
		{
			String statusCode = responseJSON.optString(ServerKeys.STATUS_CODE);
			String statusMessage = responseJSON.optString(ServerKeys.STATUS_MESSAGE);

			if (!Utilities.isNullString(statusCode))
			{
				double status = Double.valueOf(statusCode);
				if (status != HttpURLConnection.HTTP_OK && status != HttpURLConnection.HTTP_CREATED
				  && status != HttpURLConnection.HTTP_ACCEPTED)
				  throw new CTHttpError(statusMessage, Double.valueOf(statusCode));
			}
		}
	}

	public HashMap<String, String> getAdditionalHeaders()
	{
		HashMap<String, String> controllFieldsHolder = new HashMap<String, String>();
		//additionalHeaders.put(ServerKeys.DEVICE_ID, Utilities.getDeviceId());
		
		PackageManager manager = activity.getPackageManager();
		PackageInfo pInfo = null;
		try {
			pInfo = manager.getPackageInfo(activity.getPackageName(),
					PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			// return 0;
		}
		SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		String time = format.format(new Date());

		controllFieldsHolder.put(ServerKeys.API_VERSION, "1.0");
		controllFieldsHolder.put(ServerKeys.APP_VERSION, pInfo.versionName);
		controllFieldsHolder.put(ServerKeys.USER_AGENT,
				System.getProperty("http.agent"));
		controllFieldsHolder.put(ServerKeys.USER_LANGUAGE, Engine
				.getAppConfiguration().getLanguage());

		// controllFieldsHolder.put(ServerKeys.DEVICE_UNIQUE_ID,
		// Secure.getString(CTApplication.getContext().getContentResolver(),
		// Secure.ANDROID_ID));

		if (Consts.authenticationToken != null) {
			controllFieldsHolder.put(ServerKeys.USER_AUTHENTICATION_TOKEN,
					Consts.authenticationToken);
		}

		controllFieldsHolder.put(ServerKeys.DEVICE_UNIQUE_ID, Secure.getString(
				CTApplication.getContext().getContentResolver(),
				Secure.ANDROID_ID));
		controllFieldsHolder.put(ServerKeys.TIMEZONE_FORMAT, time);

		
		return controllFieldsHolder;
	}
	
	private static final SimpleDateFormat serverSDF = new SimpleDateFormat("Z HH:mm");
	private String getTimeZoneFormatForServer()
	{
		Calendar calendar = Calendar.getInstance();
		return serverSDF.format(calendar.getTime());
	}
	
	/* ***************************************
	 * ************ Error Check **************
	 * ***************************************
	 */
	
	
	
//	public boolean hasErrorWithCustomMessage (Throwable error,String message) // message for error 400 
//	{
//		if (error != null && error instanceof CTHttpError) {
//			int statusCode = ((CTHttpError) error).getStatusCode();
//			String errorMsg = ((CTHttpError) error).getErrorMsg();
//			if (RequestHandler.isRequestTimedOut(statusCode)) {
//				ErrorDialog.showMessageDialog(activity.getString(R.string.attention),
//						activity.getString(R.string.timeout), activity, this);
//			} else if (statusCode == -1) {
//				ErrorDialog.showMessageDialog(activity.getString(R.string.attention),
//						activity.getString(R.string.conn_error), activity);
//
//			} else if (statusCode == 400) {
//				ErrorDialog.showMessageDialog(activity.getString(R.string.attention),
//						"Failed to add place",activity);
//
//			} else if (!Utilities.isNullString(errorMsg)) {
//				ErrorDialog.showMessageDialog(null, errorMsg, activity);
//			} else {
//
//			}
//		}
//	}
	
	/*
	 * Setters & Getters
	 */
	public void setShowLoadingDialog(boolean isShowLoadingDialog)
	{
		this.isShowLoadingDialog = isShowLoadingDialog;
	}

	public boolean isShowLoadingDialog()
	{
		return isShowLoadingDialog;
	}
	public int getRequestID()
  {
	  return requestID;
  }
}
