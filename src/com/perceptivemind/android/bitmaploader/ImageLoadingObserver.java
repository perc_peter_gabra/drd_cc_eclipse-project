package com.perceptivemind.android.bitmaploader;

import android.graphics.drawable.BitmapDrawable;

public interface ImageLoadingObserver {

	public void handleImageLoaded(Object key, String imageUrl, BitmapDrawable loadedBitmap);
	
	public boolean isAllowConcurrentRequests();
}
