package com.perceptivemind.android.bitmaploader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.perceptivemind.template.helper.Utilities;

public class ImageConfiguration {
	private boolean isBlurred;
	private boolean isCircled;
	private int width;
	private int height;
	
	private Bitmap loadingBitmap;
	private Bitmap inBitmap;
	
	DownloadImageProcess downloadImageProcess;
	
	public ImageConfiguration(boolean isBlurred, boolean isCircled, int width,
			int height, Context context, int resId, DownloadImageProcess downloadImageProcess) {
		this.isBlurred = isBlurred;
		this.isCircled = isCircled;
		this.width = width;
		this.height = height;
		this.downloadImageProcess = downloadImageProcess;
		
		Bitmap bitmap = resId != 0? BitmapFactory.decodeResource(context.getResources(), resId) : Bitmap.createBitmap(width, height, Config.ARGB_4444);
		
		if(isCircled()){
			loadingBitmap = Utilities.getRoundedShape(bitmap, getWidth());
    	}else{
    		loadingBitmap = bitmap;
    	}
	}
	
	public void enableInBitmap(){
		inBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
	}
	
	public boolean isBlurred() {
		return isBlurred;
	}
	public void setBlurred(boolean isBlurred) {
		this.isBlurred = isBlurred;
	}
	public boolean isCircled() {
		return isCircled;
	}
	public void setCircled(boolean isCircled) {
		this.isCircled = isCircled;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public Bitmap getLoadingBitmap() {
		return loadingBitmap;
	}
	
	public void onDestory(){
		if(loadingBitmap != null && !loadingBitmap.isRecycled() && Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
			loadingBitmap.recycle();
		}
		
		if(inBitmap != null && !inBitmap.isRecycled() && Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
			inBitmap.recycle();
		}
		
		loadingBitmap = null;
		inBitmap = null;
	}

	public Bitmap getInBitmap() {
		return inBitmap;
	}

	public void setInBitmap(Bitmap inBitmap) {
		this.inBitmap = inBitmap;
	}

	public DownloadImageProcess getDownloadImageProcess()
  {
	  return downloadImageProcess;
  }
}
