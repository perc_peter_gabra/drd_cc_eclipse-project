package com.perceptivemind.android.bitmaploader;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

public abstract class DownloadImageProcess
{
	public abstract void processDownloadedImage(BufferedInputStream in, BufferedOutputStream out);
}
