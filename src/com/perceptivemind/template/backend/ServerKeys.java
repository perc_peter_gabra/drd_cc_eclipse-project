package com.perceptivemind.template.backend;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ServerKeys {
	public static final String STATUS_CODE = "statusCode";
	public static final String STATUS_MESSAGE = "statusMessage";
	
	public static final String CONTENT_TYPE_JSON = "application/json; charset=UTF-8";

	// Server Date
	public static final String SERVER_HEADER_DATE = "Date";
	public static final SimpleDateFormat SERVER_HEADER_SDF = new SimpleDateFormat(
			"EEE, dd MMM yyyy HH:mm:SS zzz", Locale.US);

	// Headers
	public static final String DEVICE_ID = "deviceId";
	public static final String HASH = "hash";

	// Controll Fields

	public static final String API_VERSION = "Api-Version";
	public static final String APP_VERSION = "App-Version";
	public static final String USER_AGENT = "User-Agent";
	public static final String USER_LANGUAGE = "User-Lang";
	public static final String USER_AUTHENTICATION_TOKEN = "Auth-Token";
	public static final String DEVICE_UNIQUE_ID = "Device-Uniqueid";
	public static final String DATA_LAST_REQUESTS = "Data-Last-Requests";
	public static final String TIMEZONE_FORMAT = "Timezone-Format";
	
	
	// Packages 
	public static final String RESPONSE = "response";
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String PRICE = "price";
	public static final String COLOR = "color";
	public static final String MONEY_SAVED = "moneySaved";
	public static final String USAGE_WEEKS = "usageWeeks";
	public static final String STATUS = "status";
	public static final String OFFERS_COUNT = "offers_count";
	public static final String OUTLETS_COUNT = "outlets_count";
	public static final String EXPIRATION = "expiration";
	public static final String ORDER_ID = "order_id";
	
	// USERS
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password"; 
	public static final String FACEBOOK_ID = "facebook_id";
	public static final String PHONE = "phone";
	public static final String CITY = "city";
	public static final String AREA = "area";
	public static final String STREET = "street";
	public static final String LANDMARKS = "landmarks";
	public static final String BUILDING = "building";
	public static final String FLOOR = "floor";
	public static final String APARTMENT = "apartment";
	


}
