package com.perceptivemind.template.managers;

import java.util.ArrayList;
import java.util.Arrays;

import com.perceptivemind.template.model.CreditCard;


public class TemplateManager {


	private ArrayList<CreditCard> cardsList; 
	protected static TemplateManager _instance = null;
	private CreditCard currentSelectedCard ;

	public static TemplateManager getInstance() {
		if (_instance == null)
			_instance = new TemplateManager();
		return _instance;
	}

	
	/*
	 * setters and getters 
	 */
	public ArrayList<CreditCard> getCardsList() {
		if(cardsList == null)
			cardsList = fillWithTempData();
		return cardsList;
	}

	public void setCardsList(ArrayList<CreditCard> cardsList) {
		this.cardsList = cardsList;
	}
	
	
	
	public CreditCard getCurrentSelectedCard() {
		if ( currentSelectedCard == null)
			currentSelectedCard = new CreditCard();
		return currentSelectedCard;
	}


	public void setCurrentSelectedCard(CreditCard currentSelectedCard) {
		this.currentSelectedCard = currentSelectedCard;
	}


	/*
	 * Update creditcard details 
	 */
	public void updateCardInList(CreditCard updatedCard)
	{	if(this.cardsList == null)
		return;
		
		for (int i = 0; i < this.cardsList.size() ; i++)
		{
			CreditCard tempCard = this.cardsList.get(i);
			if(tempCard.getId() == updatedCard.getId())
				this.cardsList.set(i, updatedCard);
		}
	}
	
	public  boolean hasEnabledCard()
	{
		for (int i = 0; i < this.cardsList.size() ; i++)
		{
			CreditCard tempCard = this.cardsList.get(i);
			if(tempCard.isEnabled())
				return true;
		}
		return false;
	}
	
	
	/*
	 * fill array with temp data
	 */
	private ArrayList<CreditCard> fillWithTempData() {
		ArrayList<CreditCard> tempArrayList = new ArrayList<CreditCard> (Arrays.asList(
				new CreditCard(1, "Carte Mozaiic M6", 500, "M6 5745 04XX XXXX XX46", true),
				new CreditCard(2, "Carte Mozaiic M6", 700, "M7 4242 04XX XXXX XX88",false),
				new CreditCard(3, "Carte Mozaiic M6", 100, "M8 8845 04XX XXXX XX46",true),
				new CreditCard(4, "Carte Mozaiic M6", 2000, "M2 5757 04XX XXXX XX46",false)
				));
		
		
		return tempArrayList;

	}
	
	
	

	

}
