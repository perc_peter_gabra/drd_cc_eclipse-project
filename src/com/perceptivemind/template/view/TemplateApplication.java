package com.perceptivemind.template.view;

import com.perceptivemind.template.CTApplication;
import com.perceptivemind.template.managers.Engine;


public class TemplateApplication extends CTApplication
{

	@Override
	public void onCreate()
	{
		super.onCreate();


		Engine.initialize(this);
		Engine.validateCachedData(this);
		Engine.setApplicationLanguage(this, Engine.getAppConfiguration().getLanguage());

	}
}
