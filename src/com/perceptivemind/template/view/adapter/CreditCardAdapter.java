package com.perceptivemind.template.view.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.perceptivemind.template.R;
import com.perceptivemind.template.model.CreditCard;


public class CreditCardAdapter extends BaseAdapter {
	
	public static final String VIEW_TAG_CARDS_LIST = "VIEW_TAG_CARDS_LIST";
	
// KEYS
	public static final int DISPLAY_ADAPTER = 0;
	public static final int CONFIRMATION_ADAPTER = 1;
	// DISPLAY to show toggle button
	// confirmation will hide the toggle button
	
	private int adapterType = 0;
	
	Context context;
	ArrayList<CreditCard> cardsList;
	OnClickListener mOnClickListener;
	OnCheckedChangeListener onCheckedChanged;	
	
	
	public CreditCardAdapter(Context context, ArrayList<CreditCard> cardsList,
			OnClickListener mOnClickListener, int adapterType , OnCheckedChangeListener onCheckedChanged) {
		super();
		this.context = context;
		this.cardsList = cardsList;
		this.mOnClickListener = mOnClickListener;
		this.adapterType = adapterType;
		this.onCheckedChanged = onCheckedChanged;
	}

	@Override
	public int getCount() {

		if (cardsList == null)
			cardsList = new ArrayList();
		
		return cardsList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		if (view == null)
			view = LayoutInflater.from(context).inflate(
					R.layout.credit_card_cell, null);
		
		CreditCard tempCard = cardsList.get(position);
		if(tempCard == null)
			return view;
		
		TextView tvCCListTitle = (TextView) view.findViewById(R.id.tvCCListTitle);
		TextView tvCCListDetails = (TextView) view.findViewById(R.id.tvCCListDetails);
		ImageView ivCCimage = (ImageView) view.findViewById(R.id.ivCCimage);
		ToggleButton ccListToggle = (ToggleButton) view.findViewById(R.id.ccListToggle);
		
		// fill data
		tvCCListTitle.setText(tempCard.getAlias());
		tvCCListDetails.setText(tempCard.getCardNumber());
		ccListToggle.setChecked(tempCard.isEnabled());
		
		if(adapterType == CONFIRMATION_ADAPTER)
		{
			ccListToggle.setVisibility(View.GONE);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					(int)context.getResources().getDimension(R.dimen.cc_confirmation_table_image_width), 
					(int)context.getResources().getDimension(R.dimen.cc_confirmation_table_image_height));
			ivCCimage.setLayoutParams(layoutParams);
		}
		
		

		ccListToggle.setTag(R.id.view_tag_cards_list_toggle,tempCard);
		ccListToggle.setOnCheckedChangeListener(onCheckedChanged);
		
		
		view.setTag(R.id.view_tag_cards_list,VIEW_TAG_CARDS_LIST);
		view.setTag(R.id.object_tag_cards_list_card_object,tempCard);
		
		view.setOnClickListener(mOnClickListener);

		return view;
	}
	

}
