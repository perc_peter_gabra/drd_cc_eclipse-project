package com.perceptivemind.template.view.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perceptivemind.template.R;

public class HeaderLayout extends LinearLayout {

	public static final String VIEW_TAG_LEFT_HEADER_BUTTON = "VIEW_TAG_LEFT_HEADER_BUTTON";
	public static final String VIEW_TAG_RIGHT_HEADER_BUTTON = "VIEW_TAG_RIGHT_HEADER_BUTTON";

	ImageButton imageButtonLeftButton, imageButtonRightButton;
	TextView textViewtitle ;
	Button buttonHeaderRightButton;

	public HeaderLayout(Context context) {
		this(context, null);
	}

	public HeaderLayout(Context context, AttributeSet attrs) {
		super(context, attrs);

		init();
	}

	private void init() {
		View view = LayoutInflater.from(getContext()).inflate(R.layout.header,
				null);
		view.setMinimumWidth(getResources().getDisplayMetrics().widthPixels);
		view.setMinimumHeight(getResources().getDimensionPixelSize(
				R.dimen.header_height));
		
		imageButtonLeftButton = (ImageButton) view
				.findViewById(R.id.imageButtonHeaderLeftButton);
		imageButtonRightButton = (ImageButton) view
				.findViewById(R.id.imageButtonHeaderRightButton);
		textViewtitle = (TextView) view.findViewById(R.id.textViewHeaderTitle);
		buttonHeaderRightButton = (Button) view.findViewById(R.id.buttonHeaderRightButton);
		

		removeAllViews();
		addView(view);
		applyFonts();

		handleButtonsEvents();
	}

	private void applyFonts() {

	}

	private void handleButtonsEvents() {
		imageButtonLeftButton.setTag(VIEW_TAG_LEFT_HEADER_BUTTON);
		imageButtonRightButton.setTag(VIEW_TAG_RIGHT_HEADER_BUTTON);
		buttonHeaderRightButton.setTag(VIEW_TAG_RIGHT_HEADER_BUTTON);

	}

	/*
	 * Right button
	 */
	public void setRightButtonClickListener(View.OnClickListener onClickListener) {
		imageButtonRightButton.setOnClickListener(onClickListener);
		buttonHeaderRightButton.setOnClickListener(onClickListener);
	}
	


	public void setRightButtonText(String text) {

		// to handel text in button  Hide image button and show Button 
		buttonHeaderRightButton.setText(text);
		buttonHeaderRightButton.setVisibility(View.VISIBLE);
		imageButtonRightButton.setVisibility(View.GONE);
	}
	
	
	public void setRightButtonImage(int resoureID)
	{
		imageButtonRightButton.setImageResource (resoureID);
		imageButtonRightButton.setVisibility(View.VISIBLE);
		buttonHeaderRightButton.setVisibility(View.GONE);
	}

	public void hideRightButton()
	{
		imageButtonRightButton.setVisibility(View.GONE);
		buttonHeaderRightButton.setVisibility(View.GONE);
	}
	public void setRigtButtonTag (String tag)
	{
		buttonHeaderRightButton.setTag(tag);
		imageButtonRightButton.setTag(tag);
	}
	/*
	 * Left button
	 */
	public void setLeftOnClickListener(View.OnClickListener onClickListener) {
		imageButtonLeftButton.setOnClickListener(onClickListener);
	}
	public void setLeftButtonVisibilty(boolean visible)
	{
		imageButtonLeftButton.setVisibility(visible?View.VISIBLE:View.GONE);
	}
	public void setLeftButtonTag (String tag)
	{
		imageButtonLeftButton.setTag(tag);
	}
	public void setLeftButtonImage(int resourceID)
	{
		imageButtonLeftButton.setImageResource(resourceID);
	}
	
	// header 

	public void setTitleTextView(String text) {
		textViewtitle.setText(text);
	}
	
	
	// header background

	public void setHeaderBackGround(int color) {
		this.setBackgroundColor(color);
	}
	

}