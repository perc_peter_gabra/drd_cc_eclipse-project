package com.perceptivemind.template.view;

import java.util.Locale;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.perceptivemind.android.bitmaploader.ImageFetcher;
import com.perceptivemind.template.R;
import com.perceptivemind.template.helper.Logger;
import com.perceptivemind.template.managers.Engine;

public abstract class TemplateBaseActivity extends FragmentActivity  {

	public enum APPMode {
		APPModeDriver, APPModePedestrian;
	}

	protected abstract void doOnCreate(Bundle arg0);

	private static final String TAG = "AutoStopBaseActivity";
	private boolean mAllowSideMenu = true;
	private int mActivityLayout;
	private LinearLayout header_Layout , bottom_layout;
	ViewFlipper viewFlipperHeader;
	private Button imageButtonBottomBarLocations , imageButtonBottomBarProfile , imageButtonBottomBarSearch , imageButtonBottomBar4;


	private DrawerLayout mDrawerLayout;
	

	public static TemplateBaseActivity activity;
	private/* static */ImageFetcher mImageFetcher;

	public TemplateBaseActivity(int activityLayout) {
		this.mActivityLayout = activityLayout;
	}
	


	@Override
	protected void onStart() {
		super.onStart();
		

		if (mImageFetcher != null) {
			mImageFetcher.initDiskCache(Engine.DataFolder.IMAGE_FETCHER_HTTP_CACHE);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (mImageFetcher != null)
			mImageFetcher
					.initDiskCache(Engine.DataFolder.IMAGE_FETCHER_HTTP_CACHE);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   //Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);



		// startIncomingView();
		Logger.instance().v("AutoStopBaseActivity", "onCreate", false);

		activity = TemplateBaseActivity.this;		
		
		setContentView(R.layout.main);
//		// content fragment
		ViewGroup contentLayout = (ViewGroup) findViewById(R.id.contentLayout);
//		// Header
		header_Layout = (LinearLayout) findViewById(R.id.header_Layout);
		// bottom
		bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
//		
		// Bottom bar buttons 
		imageButtonBottomBarLocations = (Button) findViewById(R.id.imageButtonBottomBarLocations);
		imageButtonBottomBarProfile = (Button) findViewById(R.id.imageButtonBottomBarProfile);
		imageButtonBottomBarSearch = (Button) findViewById(R.id.imageButtonBottomBarSearch);
		imageButtonBottomBar4 = (Button) findViewById(R.id.imageButtonBottomBar4);
		
		imageButtonBottomBarLocations.setBackgroundColor(Color.TRANSPARENT);
		imageButtonBottomBarProfile.setBackgroundColor(Color.TRANSPARENT);
		imageButtonBottomBarSearch.setBackgroundColor(Color.TRANSPARENT);
		imageButtonBottomBar4.setBackgroundColor(Color.TRANSPARENT);
		
		// - End of bottom
		
//		// header init()
////		headerLayout = (LinearLayout) findViewById(R.id.headerLayoutHome);
//		viewFlipperHeader = (ViewFlipper) findViewById(R.id.viewFlipperHeader);

		if (mActivityLayout != -1)
			LayoutInflater.from(this).inflate(mActivityLayout,
					contentLayout, true);



		
		handleButtonsEvents();
		doOnCreate(savedInstanceState);

		// Image fetcher
		if (mImageFetcher == null) {
			mImageFetcher = new ImageFetcher(this,
					Engine.DataFolder.IMAGE_FETCHER_HTTP_CACHE);
			mImageFetcher.addImageCache(this.getSupportFragmentManager(),Engine.getCacheParams());
		}
	}

	private void handleButtonsEvents() {
	}

	/*
	 * bottom bar
	 */
	public void  setBottomBarOnClick(OnClickListener onClickListener) {
		imageButtonBottomBarLocations.setOnClickListener(onClickListener);
		imageButtonBottomBarProfile.setOnClickListener(onClickListener);
		imageButtonBottomBarSearch.setOnClickListener(onClickListener);
		imageButtonBottomBar4.setOnClickListener(onClickListener);
	}
	
	public void  setBottomBarVisibilty(boolean visible) {
		bottom_layout.setVisibility(visible?View.VISIBLE:View.GONE);
	}
//	TODO
	protected  void locationOnClick()
	{
		//setLocationClickable(false);

//		Intent intent = new Intent(this,MapsActivity.class);
//		intent.putExtra(Consts.MAP_TYPE_IDENTIFIER, Consts.MAP_TYPE_BRANCHES_VIEW);
//		startActivity(intent);
	}
	
	protected void profileOnClick()
	{
//			startActivity(new Intent(this, ProfileActivity.class));
	}
	protected void searchOnClick()
	{
//		startActivity(new Intent(this,SearchActivity.class));
	}
	protected void chattingOnClick() {
		
	}
	protected  void setLocationClickable(boolean clickable)
	{
		Toast.makeText(this, "cliccked2", Toast.LENGTH_SHORT).show();
		imageButtonBottomBarLocations.setClickable(clickable);
		imageButtonBottomBarLocations.setBackgroundColor(clickable?Color.TRANSPARENT:Color.parseColor("#E25342"));
	}
	
	protected void setProfileClickable(boolean clickable) 
	{
		imageButtonBottomBarProfile.setClickable(clickable);
		imageButtonBottomBarProfile.setBackgroundColor(clickable?Color.TRANSPARENT:Color.parseColor("#E25342"));
	}
	protected void setSearchClickable(boolean clickable)
	{
		imageButtonBottomBarSearch.setClickable(clickable);
		imageButtonBottomBarSearch.setBackgroundColor(clickable?Color.TRANSPARENT:Color.parseColor("#E25342"));
	}
	
	/*
	 * Set header layout 
	 */
	protected void setHeaderLayout (View headerView)
	{
		if(header_Layout != null)
		{
			header_Layout.setVisibility(View.VISIBLE);
			header_Layout.removeAllViews();
			header_Layout.addView(headerView);
		}
	}

	/*
	 * Add new fragment and empty the stack of fragments
	 */
	public void addFragmentAndEmptyStack(Fragment fragment, boolean isAnimated) {
		FragmentManager manager = getSupportFragmentManager();
		Logger.instance().v("addFragmentAndEmptyStack",
				"Count " + manager.getBackStackEntryCount(), false);
		if (manager.getBackStackEntryCount() > 0)
			for (int i = 0; i < manager.getBackStackEntryCount(); i++)
				getSupportFragmentManager().popBackStack();

		addFragment(fragment, true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
				isAnimated);
	}

	public void addFragment(Fragment fragment, boolean addToBackStack,
			int transition) {
		addFragment(fragment, addToBackStack, transition, true);
	}

	public void addFragment(Fragment fragment, boolean addToBackStack,
			int transition, boolean isAnimated) {
		Log.v("addFragment", addToBackStack + " " + fragment);

	}

	public void finishFragmentOrActivity() {
		FragmentManager manager = getSupportFragmentManager();
		Logger.instance().v("finishFragmentOrActivity",
				manager.getBackStackEntryCount(), false);
		if (manager.getBackStackEntryCount() > 0)
			getSupportFragmentManager().popBackStack();
		else {
			moveTaskToBack(true);
			// if (HomeTabsActivity.tabsActivity != null)
			// {
			// HomeTabsActivity.tabsActivity.moveTaskToBack(true);
			// HomeTabsActivity.tabsActivity.finish();
			// }
			// Utilities.exit();

			// finish();
		}
	}

	public void popToRoot() {
		runOnUiThread(new Runnable() {
			public void run() {
				int length = getSupportFragmentManager()
						.getBackStackEntryCount() - 1;
				Logger.instance().v("Poptoroot - length", length, false);
				if (length > 0) {
					for (int i = 0; i < length; i++)
						getSupportFragmentManager().popBackStack();
				}
			}
		});
	}

	public void launchNewFragment(View v, Fragment fragment) {
		// Fragment anotherFragment = Fragment.instantiate(this,
		// ExampleFragment.class.getName());
		addFragment(fragment, true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	}

	public void launchNewFragment(Fragment fragment, boolean isAnimated) {
		// Fragment anotherFragment = Fragment.instantiate(this,
		// ExampleFragment.class.getName());
		addFragment(fragment, true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
				isAnimated);
	}

	public void removeFragment(Fragment fragment) {
		getSupportFragmentManager().beginTransaction().remove(fragment)
				.commit();
	}


	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Logger.instance().v("ONKEYDOWN", (keyCode == KeyEvent.KEYCODE_BACK),
				false);
		if (keyCode == KeyEvent.KEYCODE_BACK && onBackButtonPressed()) {
			return true;
//			 getFragmentManager().popBackStackImmediate(null,
//			 FragmentManager.POP_BACK_STACK_INCLUSIVE);

		}
		return super.onKeyDown(keyCode, event);
	}

	public boolean onBackButtonPressed() {
		Log.v("FragmentStackAct", "Back "
				+ getSupportFragmentManager().getBackStackEntryCount() + "  ");

		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			// getSupportFragmentManager().popBackStack();
			//callBack();
			return true;
		} else {
			// if (HomeTabsActivity.tabsActivity != null)
			// {
			// HomeTabsActivity.tabsActivity.moveTaskToBack(true);
			// HomeTabsActivity.tabsActivity.finish();
			// }
			// finish();
			// moveTaskToBack(true);
		}
		return false;
	}

	/*
	 * Handle open/close menu
	 */
	public void handleMenuAppearance() {
		if (mDrawerLayout.isDrawerOpen(Gravity.END))
			mDrawerLayout.closeDrawer(Gravity.END);
		else
			mDrawerLayout.openDrawer(Gravity.END);
	}

	/*
	 * Open Menu Panel
	 */
	public void openMenuPanel() {
		mDrawerLayout.openDrawer(GravityCompat.END);
	}



	public ImageFetcher getImageFetcher() {
		return mImageFetcher;
	}

	public static int indexOfSearchQuery(String search, String displayName) {
		if (!TextUtils.isEmpty(search)) {
			return displayName.toLowerCase(Locale.getDefault()).indexOf(
					search.toLowerCase(Locale.getDefault()));
		}
		return -1;
	}

	public void openUrl(String url) {
		final Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		// startActivity(Intent.createChooser(intent,
		// getString(R.string.complete_action)));
	}



//	public void setHeaderView(LinearLayout headerLLayout)
//	{
//		mActivityHeader = headerLLayout;
//		if (mActivityHeader != null)
//		{
//			this.headerLayout.setVisibility(View.VISIBLE);
//			this.headerLayout.addView(mActivityHeader);
//		}
//
//	}

	
	/*
	 *  0 means no tag
	 */
	public boolean isNullViewTagWithId(View v ,int tagID)
	{
		
			if (tagID == 0) // with no tag Id
			{
				if (v.getTag() != null)
					return false;
			}
			else
			{
				if (v.getTag(tagID) != null)
					return false;
			}

		return true;
	}
	

}
