package com.perceptivemind.template.view;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;

import com.perceptivemind.template.R;
import com.perceptivemind.template.managers.TemplateManager;
import com.perceptivemind.template.model.CreditCard;
import com.perceptivemind.template.view.adapter.CreditCardAdapter;

public class CreditCardsListActivity extends TemplateBaseActivity implements OnClickListener, OnCheckedChangeListener {


	ListView listViewCreditCardsList;
	Button buttonValidate;
	CreditCardAdapter creditCardAdapter;
	ArrayList<CreditCard> cardsList; 
	
	public CreditCardsListActivity() {
		super(R.layout.cards_list);
	}

	@Override
	protected void doOnCreate(Bundle arg0) {


		setBottomBarVisibilty(false);
		
		// button has selector it change color according to enabled property 
		buttonValidate = (Button) findViewById(R.id.buttonValidate);
		listViewCreditCardsList = (ListView) findViewById(R.id.listViewCreditCardsList);
		refreshTable();

	}
	@Override
	protected void onResume() {
		super.onResume();
		refreshTable();
		checkValidateButtonAvilabilty();
	}
	
	private void refreshTable()
	{
		cardsList = TemplateManager.getInstance().getCardsList();
		
		if(creditCardAdapter == null)
			{
				creditCardAdapter = new CreditCardAdapter(this, cardsList, this,CreditCardAdapter.DISPLAY_ADAPTER,this);
				listViewCreditCardsList.setAdapter(creditCardAdapter);
			}
		else 
			creditCardAdapter.notifyDataSetChanged();
		
			
	}
	private void checkValidateButtonAvilabilty()
	{
		// if there is any enabled card button will be enabled 
		buttonValidate.setEnabled(TemplateManager.getInstance().hasEnabledCard());
			
	}
	/*
	 * Start activity edit card
	 */
	
	private void startEditActivity(CreditCard tempCard)
	{
		TemplateManager.getInstance().setCurrentSelectedCard(tempCard);
		startActivity(new Intent(CreditCardsListActivity.this, CardDetailsActivity.class));
	}
	
	

	@Override
	public void onClick(View v) {
		if(v.getTag(R.id.view_tag_cards_list)!= null)
		{
			if (v.getTag(R.id.view_tag_cards_list).toString().equals(CreditCardAdapter.VIEW_TAG_CARDS_LIST))
			{
				if(v.getTag(R.id.object_tag_cards_list_card_object)!= null)
				{
					CreditCard tempCard = (CreditCard) v.getTag(R.id.object_tag_cards_list_card_object);
					startEditActivity(tempCard);
				}
			}
		}
			
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(buttonView.getTag(R.id.view_tag_cards_list_toggle) != null)
		{
			CreditCard tempCard = (CreditCard) buttonView.getTag(R.id.view_tag_cards_list_toggle);
			tempCard.setEnabled(isChecked);
			TemplateManager.getInstance().updateCardInList(tempCard);
			checkValidateButtonAvilabilty();
		}
		
	}

}
