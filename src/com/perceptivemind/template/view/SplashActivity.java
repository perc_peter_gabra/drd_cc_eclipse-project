package com.perceptivemind.template.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.perceptivemind.template.R;
import com.perceptivemind.template.view.fragment.LoadingPageFragment;

public class SplashActivity extends TemplateBaseActivity {

	private static final int TIME_OUT = 2 * 1000;// in Millis
	Handler redirectHandler;
	Runnable redirectRunnable;
	
	LinearLayout llSplashChild;
	
	// FRAGMENT MANAGER
		FragmentManager fragManager;
		FragmentTransaction fragTransaction;
		LoadingPageFragment loadingPageFragment;


	public SplashActivity() {
		super(R.layout.splash);
	}

	@Override
	protected void doOnCreate(Bundle arg0) {
		
		setBottomBarVisibilty(false);
		
		
		llSplashChild = (LinearLayout) findViewById(R.id.llSplashChild);
		// fragment
		fragManager = getSupportFragmentManager();
		
		loadingPageFragment = LoadingPageFragment.newInstance(LoadingPageFragment.FRAGMENT_SPLASH);
		fragTransaction = fragManager.beginTransaction();
		fragTransaction.add(llSplashChild.getId(), loadingPageFragment, "");
		// set user visibility for the previous fragment
		loadingPageFragment.setUserVisibleHint(true);
		fragTransaction.commit();
		

		// Wait X seconds and redirect
		redirectHandler = new Handler();
		redirectRunnable = new Runnable()
		{
			@Override
			public void run() {
				redirect();
			}
		};
		
		redirectHandler.postDelayed(redirectRunnable, TIME_OUT);
	}

	/*
	 * Redirect to home page
	 */
	private void redirect() {
		 startActivity(new Intent(SplashActivity.this, CreditCardsListActivity.class));
		 finish();
	}

}
