package com.perceptivemind.template.view;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.perceptivemind.template.R;
import com.perceptivemind.template.helper.Utilities;
import com.perceptivemind.template.managers.TemplateManager;
import com.perceptivemind.template.model.CreditCard;

public class CardDetailsActivity extends TemplateBaseActivity implements
		OnClickListener {

	Button buttonValidate;
	ImageView ivCCimage, ivEditAlias, ivCancelEdit, ivConfirmEdit;
	TextView tvCCDetailsTitle, tvCCListDetailsDesc, tvCardAlias;
	ToggleButton ccDetailsToggle;
	EditText etCardAlias, etAmount;
	LinearLayout llDetailsPopup;
	CreditCard currentSelectedCard;

	public CardDetailsActivity() {
		super(R.layout.card_details);
	}

	@Override
	protected void doOnCreate(Bundle arg0) {


		// buttons has selector it change color according to enabled property 
		setBottomBarVisibilty(false);
		initUIComponenets();

	}

	private void initUIComponenets() {
		buttonValidate = (Button) findViewById(R.id.buttonValidate);
		ivCCimage = (ImageView) findViewById(R.id.ivCCimage);
		ivEditAlias = (ImageView) findViewById(R.id.ivEditAlias);
		ivCancelEdit = (ImageView) findViewById(R.id.ivCancelEdit);
		ivConfirmEdit = (ImageView) findViewById(R.id.ivConfirmEdit);
		tvCCDetailsTitle = (TextView) findViewById(R.id.tvCCDetailsTitle);
		tvCCListDetailsDesc = (TextView) findViewById(R.id.tvCCListDetailsDesc);
		tvCardAlias = (TextView) findViewById(R.id.tvCardAlias);
		ccDetailsToggle = (ToggleButton) findViewById(R.id.ccDetailsToggle);
		etCardAlias = (EditText) findViewById(R.id.etCardAlias);
		etAmount = (EditText) findViewById(R.id.etAmount);
		llDetailsPopup = (LinearLayout) findViewById(R.id.llDetailsPopup);

		ivEditAlias.setOnClickListener(this);
		ivCancelEdit.setOnClickListener(this);
		ivConfirmEdit.setOnClickListener(this);
		buttonValidate.setOnClickListener(this);
		fillData();
	}
	
	private void fillData() {
		currentSelectedCard = TemplateManager.getInstance().getCurrentSelectedCard();
		
		ccDetailsToggle.setChecked(currentSelectedCard.isEnabled());
		tvCardAlias.setText(currentSelectedCard.getAlias());
		etAmount.setText(currentSelectedCard.getAmount()+"");
		
		
	}

	/*
	 * Set popup visibity
	 */
	private void handlePopUpVisibilty(boolean visible) {
		llDetailsPopup.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	/*
	 * Edit Alias
	 */
	private void editAlias() {
		etCardAlias.setText(currentSelectedCard.getAlias());
		handlePopUpVisibilty(true);
	}

	/*
	 * Cancel popup
	 */
	private void popupCancel() {
		handlePopUpVisibilty(false);
	}

	/*
	 * confirm popup
	 */
	private void popupConfirm() {
		currentSelectedCard.setAlias(etCardAlias.getText().toString());
		tvCardAlias.setText(currentSelectedCard.getAlias());
		
		handlePopUpVisibilty(false);
	}
	
	/*
	 * Save and finish
	 */
	private void saveAndFinish() {
		currentSelectedCard.setEnabled(ccDetailsToggle.isChecked());
		
		if(Utilities.isDouble(etAmount.getText().toString()))
			currentSelectedCard.setAmount(Double.parseDouble(etAmount.getText().toString()));
		
		TemplateManager.getInstance().updateCardInList(currentSelectedCard);
		finish();
		

	}

	
	@Override
	public void onBackPressed() {
		
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivEditAlias:
			editAlias();
			break;
		case R.id.ivCancelEdit:
			popupCancel();
			break;
		case R.id.ivConfirmEdit:
			popupConfirm();
			break;
		case R.id.buttonValidate:
			saveAndFinish();
			break;

		default:
			break;
		}

	}

}
