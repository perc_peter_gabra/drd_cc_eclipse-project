package com.perceptivemind.template.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.perceptivemind.template.R;

public class LoadingPageFragment extends TemplateBaseFragment {
	// KEYS
	public static final String FRAGMENT_TYPE = "ACTIVITY_TYPE";
	public static final int FRAGMENT_SPLASH = 0;
	public static final int FRAGMENT_LOADING = 1;
	public static final int FRAGMENT_CONNECTION_ERROR = 2;
	// ACTIVITY TYPE
	private int fragmentType = 0;


	TextView tvConnectionError1, tvConnectionError2;
	ImageView ivLogo;
	ProgressBar progressBarSplash;
	View view;
	
	//
	Handler handler;

	public static LoadingPageFragment newInstance(int fragmentType) {
		LoadingPageFragment fragment = new LoadingPageFragment();
		fragment.fragmentType = fragmentType;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (view != null) {
			ViewParent oldParent = view.getParent();
			if (oldParent != container) {
				((ViewGroup) oldParent).removeView(view);
			}
		} else {
			
			view = LayoutInflater.from(getActivity()).inflate(
					R.layout.loading_fragment, container, false);

			tvConnectionError1 = (TextView) view.findViewById(R.id.tvConnectionError1);
			tvConnectionError2 = (TextView) view.findViewById(R.id.tvConnectionError2);
			ivLogo = (ImageView) view.findViewById(R.id.ivLogo);
			progressBarSplash = (ProgressBar) view.findViewById(R.id.progressBarSplash);
			
			

			if(fragmentType == FRAGMENT_SPLASH)
			{
				ivLogo.setVisibility(View.VISIBLE);
				tvConnectionError1.setVisibility(View.GONE);
				tvConnectionError2.setVisibility(View.GONE);
				progressBarSplash.setVisibility(View.GONE);

			}
			else if ( fragmentType == FRAGMENT_LOADING)
			{
				ivLogo.setVisibility(View.GONE);
				tvConnectionError1.setVisibility(View.GONE);
				tvConnectionError2.setVisibility(View.GONE);
				progressBarSplash.setVisibility(View.VISIBLE);
			}
			else {// connection error 
				ivLogo.setVisibility(View.GONE);
				tvConnectionError1.setVisibility(View.VISIBLE);
				tvConnectionError2.setVisibility(View.VISIBLE);
				progressBarSplash.setVisibility(View.GONE);
			}

			
		}
		return view;
	}


}
