package com.perceptivemind.template.view;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.perceptivemind.template.R;
import com.perceptivemind.template.view.adapter.CreditCardAdapter;

public class ConfirmationActivity extends TemplateBaseActivity implements OnClickListener {


	ListView listViewCofirmation;
	Button btnConfirmationValidate;

	public ConfirmationActivity() {
		super(R.layout.edit_confirmation);
	}

	@Override
	protected void doOnCreate(Bundle arg0) {


		setBottomBarVisibilty(false);

		// button has selector it change color according to enabled property 
		
		btnConfirmationValidate = (Button) findViewById(R.id.btnConfirmationValidate);
		listViewCofirmation = (ListView) findViewById(R.id.listViewCofirmation);
		CreditCardAdapter creditCardAdapter = new CreditCardAdapter(this, new ArrayList(), this, CreditCardAdapter.CONFIRMATION_ADAPTER,null);
		listViewCofirmation.setAdapter(creditCardAdapter);

	}

	@Override
	public void onClick(View v) {
		
	}

}
