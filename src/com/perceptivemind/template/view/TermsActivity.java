package com.perceptivemind.template.view;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.perceptivemind.template.R;

public class TermsActivity extends TemplateBaseActivity implements OnClickListener {


	Button buttonTermsCancel;

	public TermsActivity() {
		super(R.layout.terms);
	}

	@Override
	protected void doOnCreate(Bundle arg0) {


		setBottomBarVisibilty(false);
		buttonTermsCancel = (Button) findViewById(R.id.buttonTermsCancel);
		buttonTermsCancel.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonTermsCancel:
			
			break;

		default:
			break;
		}
		
	}

}
