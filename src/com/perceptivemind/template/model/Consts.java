package com.perceptivemind.template.model;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Consts {
	// Prefs
	public static final String PREF_FILE_NAME = "GBA14PrefFile";

	// is debugabble -> used for push
	public static final boolean IS_DEBUGGABLE = true;

	// Flurry integration
	public static final boolean IS_FLURRY_ENABLED = true;
	public static final String FLURRY_API_KEY = "BPSRJP3X96RCWDMJMPCT";

	// Event time
	public static final String EVENT_TIME = "15-09-2014 09:00:00";
	public static final SimpleDateFormat EVENT_TIME_SDF = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm:SS", Locale.ENGLISH);

	// Servers
	public static final boolean IS_PRODUCTION = false;

	public static final String SERVICES_SALT = "pHw85vDT/12";

	// Social URLs
	public static final String URL_SOCIAL_FACEBOOK = "";
	public static final String URL_SOCIAL_TWITTER = "";
	public static final String URL_SOCIAL_YOUTUBE = "";

	// URLs
	private static final String URL_PRODUCTION_CMS = "";
	private static final String URL_STAGING_CMS = "http://partners.more-egypt.com/api/v1";

	public static final String URL_SERVER_CMS = IS_PRODUCTION ? URL_PRODUCTION_CMS
			: URL_STAGING_CMS;

	public static final String SERVICES_BRANCHES = "/branches";
	public static final String SERVICES_CATEGORIES = "/categories";
	public static final String SERVICES_AREAS = "/areas";
	public static final String SERVICES_PURCHASE_PACKAGE = "/purchase-package";
	public static final String SERVICES_ACTIVATE_ORDER = "/activate-order";
	public static final String SERVICES_PACKAGES = "/packages";
	public static final String SERVICES_PACKAGE_OFFERS = "/package-offers";
	public static final String SERVICES_ORDER_OFFERS = "/order-offers";
	public static final String SERVICES_MY_USED_OFFERS = "/my-used-offers";
	public static final String SERVICES_PACKAGE_ID = "?package_id";
	public static final String SERVICES_PACKAGE_ID_1 = "package_id";
	public static final String SERVICES_ORDER_ID = "order_id";

	public static final String SERVICES_LOGIN = "/login";
	public static final String SERVICES_LOCATION = "/location";
	public static final String SERVICES_OFFERS = "/offers";
	public static final String SERVICES_FAVORITE = "/favorites";
	public static final String SERVICES_RANDOM = "random";
	public static final String SERVICES_ALPHABETICAL = "alphabetical";
	public static final String SERVICES_FORMAT = "format";
	public static final String SERVICES_USER_ID = "user_id";
	public static final String SERVICES_ACTIVATION_CODE = "activation_code";
	public static final String SERVICES_CATEGORY_ID = "category_id";
	public static final String SERVICES_SUBCATEGORY_ID = "subcategory_id";
	public static final String SERVICES_AREA_ID = "area_id";
	public static final String MAP_TYPE_IDENTIFIER = "MapType";
	public static final String MAP_TYPE_OUTLET = "outlet";
	public static final String MAP_TYPE_BRANCHES_VIEW = "branchesView";
	public static final String ONLINE_PURCHASE_LINK = "http://partners.more-egypt.com/order";
	// public static final String SERVICES_ADD = "/add";
	public static final String SERVICES_FAVORITE_OFFER = "/favorite-offer";
	public static final String SERVICES_OFFER_ID = "offer_id";
	public static final String SERVICES_UPDATE_USER = "/update-user";
	public static final String SERVICES_REGISTER = "/register";
	public static final String SERVICES_REDEEM_OFFER = "/redeem-offer";
	public static final String SERVICES_RESET_PASSWORD = "/reset-password";
	public static final String SERVICES_ACTION = "action";
	public static final String SERVICE_EXPIRED = "expired";
	public static final String SERVICE_ACTIVE = "active";
	public static final String SERVICE_PENDING = "pending_activation";
	public static final String SERVICE_OUTLET_CODE = "outlet_code";
	public static final String SERVICE_ORDER_ID = "order_id";
	

	// Facebook
	public static final String FB_LINK = "http://www.matchatmobile.com/matchat/img/logo.png";
	public static final String FB_APPLICATION_NAME = "More";

	// Prefs
	public static final String PREF_USER_FILE = "prefUserFile";

	public static final String PREF_KEY_TOKEN = "prefKeyToken";

	// Padding
	public static final int TOAST_PADD_BOTTOM = 20;

	// AutoCompelete address
	public static final String PLACES_API_AUTOCOMPLETION = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
	public static final String INPUT = "input";
	public static final String KEY = "key";
	public static final String SENSOR = "sensor";
	public static final String PLACES_API_AUTOCOMPLETION_KEY = "AIzaSyAgP-daOqMm_PVwhYo9LR6cnU5QkcrYXSM";

	// authentication part
	public static String authenticationToken = null;

	// Camera Zoom level , Sets the zoom
	public static final int CAMERA_ZOOM_LEVEL = 12;
	// Camera bearing , Sets the orientation of the camera
	public static final int CAMERA_BEARING = 0;

	public static final String KEYS_IGNORE_VALIDATION = "KEYS_IGNORE_VALIDATION";

	public static final int REQUEST_USER_PACKAGES = -55480;

	public static void setAuthenticationToken(String authenticationToken) {
		Consts.authenticationToken = authenticationToken;
	}

}
