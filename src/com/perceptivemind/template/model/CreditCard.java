package com.perceptivemind.template.model;

public class CreditCard {
	
	private int id ;
	private String alias;
	private double amount;
	private String cardNumber;
	private boolean isEnabled;
	
	
	public CreditCard(int id, String alias, double amount, String cardNumber ,boolean isEnabled) {
		super();
		this.id = id;
		this.alias = alias;
		this.amount = amount;
		this.isEnabled = isEnabled;
		this.cardNumber = cardNumber;
	}


	public CreditCard() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Setters getters 
	 */


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAlias() {
		return alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public boolean isEnabled() {
		return isEnabled;
	}


	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}


	public String getCardNumber() {
		return cardNumber;
	}


	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	
	
	
	
}
